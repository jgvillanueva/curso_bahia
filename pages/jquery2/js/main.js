$( document ).ready(function() {
    console.log("Página cargada");


    var data = [
        {
            nombre: "Apartado 1",
            contenido: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sem est, dictum eget commodo at, volutpat quis tortor. Maecenas ultrices vehicula ante finibus elementum. Vivamus nec nisi sit amet mi tristique vestibulum. Ut pharetra erat nisl, vel dictum purus rutrum ut. Maecenas mollis sagittis ultricies.'
        },
        {
            nombre: "Apartado 2",
            contenido: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sem est, dictum eget commodo at, volutpat quis tortor. Maecenas ultrices vehicula ante finibus elementum. Vivamus nec nisi sit amet mi tristique vestibulum. Ut pharetra erat nisl, vel dictum purus rutrum ut. Maecenas mollis sagittis ultricies.'
        },
        {
            nombre: "Apartado 3",
            contenido: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sem est, dictum eget commodo at, volutpat quis tortor. Maecenas ultrices vehicula ante finibus elementum. Vivamus nec nisi sit amet mi tristique vestibulum. Ut pharetra erat nisl, vel dictum purus rutrum ut. Maecenas mollis sagittis ultricies.'
        }
    ]

    $('.content1').acordeon_plugin({
        data: data,
        efecto: 'slide',
        useCss: true
    });

    $('.content2').acordeon_plugin({
        data: data,
        efecto: 'slide',
        useCss: false
    });

    $().modal_plugin();
});



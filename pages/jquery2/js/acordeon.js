(function($){
    $.fn.acordeon_plugin = function(options){
        var settings = $.extend({
            data: [],
            efecto: 'slide',
            useCss: false
        }, options)

        var $container = $('<div class="acordeon_plugin"></div>');
        var $ul = $('<ul></ul>');

        if(settings.useCss){
            $container
                .addClass('useCss');
        }

        this.$elementoAbierto = null;

        settings.data.map(function(item){
            var $header = $('<h3 class="acordeon_header"></h3>')
                .text(item.nombre)
                .on('click', function(){
                    if(!settings.useCss){
                        if($(this).parent().hasClass('closed')){
                            slideOpen($(this).parent());
                        }else{
                            slideClose($(this).parent());
                        }
                    } else {
                        if($(this).parent().hasClass('closed')){
                            slideOpenCSS($(this).parent());
                        }else{
                            slideCloseCSS($(this).parent());
                        }
                    }


                })
            var $content = $('<div class="acordeon_content"></div>')
                .html(item.contenido);
            var $li = $('<li class="acordeon_bloque closed"></li>')
                .append($header)
                .append($content)
                .addClass('closed')


            $ul.append($li);
        })

        var $el = $($ul.find('.acordeon_bloque').get(0));
        setTimeout(function(){
            if(!settings.useCss) {
                slideOpen($el);
            } else {
                slideOpenCSS($el);
            }
        }, 500)

        $container
            .append($ul);



        this
            .append($container);

        return this;
    }

    function slideOpen($element){
        if(this.$elementoAbierto){
            slideClose(this.$elementoAbierto);
        }
        var $content = $('.acordeon_content', $element);
        $content
            .stop()
            .animate({
                height: $content.get(0).scrollHeight + 'px'
            }, 600, 'easeInOutCirc')
            .animate({
                left: 0
            }, 600, 'easeInOutCirc')
            //.slideDown();
            /*.css({
                'height': 'auto'
            })*/
        this.$elementoAbierto = $element;
        $element.removeClass('closed');
    }
    function slideClose($element){
        console.log($('.acordeon_content', $element));
        var $content = $('.acordeon_content', $element);
        $content
            .stop()
            .animate({
                left: '-100vw'
            }, {
                duration: 500,
                easing: 'easeInCirc',
                queue: false
            } )
            .animate({
                height: '0px'
            }, {
                duration: 500,
                easing: 'easeInCirc',
                queue: false
            })
            //.slideUp();
            /*.css({
                'height': '0px'
            })*/
        $element.addClass('closed');
    }

    function slideOpenCSS($element){
        if(this.$elementoAbierto ){
            this.$elementoAbierto.addClass('closed');
        }
        $element.removeClass('closed');
        this.$elementoAbierto = $element;
    }

    function slideCloseCSS($element){
        $element.addClass('closed');
        this.$elementoAbierto = null;
    }

})($)
(function($){
    $.fn.modal_plugin = function(){

        this.$container = $('<div class="modal_plugin"></div>');
        var $contentWindow = $('<div class="modal_container"></div>');
        var $content = $('<div class="modal_content">bubububububub</div>');
        this.$button = $('<button class="modal_close btn">Cerrar</button>')
            .on('click', cerrar.bind(this))

        $contentWindow
            .append($content)
            .append(this.$button);

        this.$container
            .append($contentWindow);

        $('body')
            .append(this.$container);

        setTimeout(function(){
            $content
                .addClass('visible')
            $contentWindow
                .addClass('visible')
        }, 1)


        return this
    }

    function cerrar(){
        this.$button.off('click', cerrar)
        this.$container.remove();
    }

})($)
$( document ).ready(function() {
    console.log("Página cargada");

    function render(content){
        $('#result_container').append('<p>'+content+'</p>');
    }

    var poligono1 = new Poligono(10, 20);
    var poligono2 = new Poligono(20, 30);

    render(poligono1.getArea());
    render(poligono2.getArea());

    var caja1 = new Caja(30, 100, 2);
    caja1.render();
    var caja2 = new Caja(130, 200, 8);
    caja2.render();
});


function Poligono(alto, _ancho){
    this.alto = alto;
    this.ancho = _ancho;
}
Poligono.prototype.getArea = function(){
    return this.alto * this.ancho;
}

function Caja(_alto, _ancho, _border){
    Poligono.call(this, _alto, _ancho);
    this.border = _border;
}
Caja.prototype = Object.create(Poligono.prototype);
Caja.prototype.render = function(){
    var $div = $('<div class="caja">'+this.getArea()+'</div>');
    $div.css({
        borderWidth: this.border+'px',
        width: this.ancho,
        height: this.alto
    })
    $('#result_container').append($div);
}



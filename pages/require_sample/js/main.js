$( document ).ready(function() {
    console.log("Página cargada");

});


requirejs.config({
    baseUrl: 'js/clases',
    paths: {
    },
    shim:{
        app: {
            exports: 'app',
            deps: ['item']
        },
        item: {
            exports: 'item',
        },
    }
});

requirejs(['app'], function(App){
    App.init();
})
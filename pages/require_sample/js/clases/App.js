

define('app', ['item'], function(Item){
    return{
        init: function(){
            console.log("Hola mundo");
            $content = $('.content ul');
            var items = [
                {nombre: 'Jorge', id: 1},
                {nombre: 'Ana', id: 2},
                {nombre: 'Juan', id: 3},
                {nombre: 'Paco', id: 4},
                {nombre: 'Cristina', id: 5},
                {nombre: 'Claudia', id: 6},
                {nombre: 'Carmen', id: 7},
                {nombre: 'Javier', id: 8},
                {nombre: 'Óscar', id: 9},
            ]
            for(var i=0; i<items.length; i++){
                var item = Item.render(items[i]);
                $content.append(item)
            }
        }
    }
})


define('circle', ['lodash'], function(_){
    return{
        getCircle: function(radius){
            return new Circle(radius);
        }
    }
})

function Circle(_radio){
    this.radio = _radio;
}
Circle.prototype.getArea = function(){
    return _.floor(Math.PI*Math.pow(this.radio, 2));
}
Circle.prototype.render = function(){
    var $div = $('<div class="circle">'+this.getArea()+'</div>');
    $div.css({
        width: this.radio*2 + 'px',
        height: this.radio*2 + 'px',
        borderRadius: this.radio + 'px',
        lineHeight: this.radio*2 + 'px',
    })
    return $div;
}
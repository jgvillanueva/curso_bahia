$( document ).ready(function() {
    console.log("Página cargada");




});


requirejs.config({
    baseUrl: 'js/clases',
    paths: {
        lodash: '../lib/lodash'
    },
    shim:{
        app: {
            exports: 'app',
            deps: ['circle', 'utilidades']
        },
        circle: {
            exports: 'circle',
            deps: ['lodash']
        },
        utilidades: {
            exports: 'utilidades'
        },
        lodash: {
            exports: 'lodash'
        },
    }
});

requirejs(['app'], function(App){
    App.init();
})
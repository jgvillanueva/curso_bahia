$( document ).ready(function() {
    console.log("Página cargada");

    function render(content){
        $('#result_container').append(content);
    }

    var $h1 = $('<h1>Hola Mundo</h1>')
        .addClass('miClase');

    render($h1);

    var myObject = myObject || {
        valuePadre: 'objeto',
        getValue: function(){
            return this.valuePadre;
        }
    }
    myObject.objetoHijo = myObject.objetoHijo || {
        valueHijo: 'objeto hijo',
        getValue: function(){
            return this.valuePadre + ' - ' + this.valueHijo;
        }
    }



    var $p = $('<p></p>')
        .html(myObject.getValue());
    render($p);

    render(myObject.objetoHijo.getValue());



    /*
    módulos
     */
    var miModulo = (function(window){
        var _valuePadre = 'módulo';
        var _getValuePadre = function(){
            return _valuePadre;
        }
        var _getContext = function(){
            return this;
        }
        var _miModuloHijo = (function(window){
            var _valueHijo = 'hijo';
            var _getValueHijo = function(){
                return _valueHijo;
            }
            return {
                getValueHijo: function(){
                    return _getValueHijo();
                },
                getValue: function(){
                    return _getValuePadre() + " - " + _getValueHijo();
                }
            }
        })(window)
        return {
            getValuePadre: function(){
                return _getValuePadre();
            },
            getContext: function(){
                return _getContext();
            },
            miModuloHijo: _miModuloHijo
        }
    })(window)
    render('<p>' + miModulo.getValuePadre() + '</p>');
    console.log(miModulo.getContext());
    render('<p>' + miModulo.miModuloHijo.getValue() + '</p>');
    render('<p>' + miModulo.miModuloHijo.getValueHijo() + '</p>');

    function miContexto(){
        miValor = '100';
        return {
            funcionInterna: function(){
                console.log(this);
            }
        }
    }
    var funcion = miContexto().funcionInterna.bind(this)
    console.log(funcion());
    console.log(miContexto().funcionInterna());

});

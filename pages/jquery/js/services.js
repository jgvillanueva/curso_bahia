(function($, window){
    $.fn.services_plugin = function(options){

        var settings = $.extend({
            urlBase: '',
            method: 'get',
            dataType: 'json',
        }, options)

        console.log(settings);
        return{
            load_data: function(_url, _callback){
                $.ajax(
                    {
                        url: settings.urlBase + _url,
                        dataType: settings.dataType,
                        method: settings.method
                    }
                )
                    .done(function(data){
                        _callback(data);
                    })
                    .always(function(result){
                        chackStatus(result);
                    })
                return this;
            },

            load_multiple: function(lista){
                var index = 0;
                function load(){
                    console.log('llamando al servidor');
                    $.ajax(
                        {
                            url: lista[index].url,
                            dataType: settings.dataType,
                            method: settings.method
                        }
                    )
                        .done(function(data){
                            console.log(lista[index]);
                            if(lista[index].callback != undefined){
                                lista[index].callback(data);
                            }
                            if(index < lista.length -1){
                                index = index + 1;
                                load()
                            }
                        })
                        .always(function(result){
                            chackStatus(result);
                        })
                }
                load();
                return this;
            }
        }



        function chackStatus(result){
            switch(result.status){
                case 404:
                    alert('No se ha encontrado la página');
                    break;
                case 500:
                    alert('Ha habidop un error en el servidor');
                    break;
            }
        }
    }

    $.fn.services_plugin.test = function(){
        $.ajax(
            {
                url: "http://dev.contanimacion.com/birds/public/getBirds/1",
            }
        )
            .always(function(result){
                if(result.status !== undefined ){
                    console.log("La url genera un error "+ result.status);
                }else{
                    console.log("Carga correctamente");
                }
            })
    }
})($, window)
(function($){
    $.fn.tabla_plugin = function(options){
        var settings = $.extend({
            data: []
        }, options)
        console.log(settings.data);

        collection = settings.data;

        $header = '<h2>Datos recibidos</h2>';
        $ul = $('<ul></ul>');
        $container = $('<div class="tabla_plugin"></div>')
            .append($header)

        this.append($container);

        var creaBotonera = _creaBotonera.bind(this);
        creaBotonera();

        $container
            .append($ul);

        var render = _render.bind(this);
        render(settings.data);



        return this;
    }

    function _creaBotonera(){
        var $botonera = $('<div class="tabla_plugin_botonera"></div>');
        var $b_clean = $('<button class="b_clean btn">Clean</button>');
        var $b_load = $('<button class="b_load btn">Load</button>');
        var $b_filter = $('<button class="b_filter btn">Filtrar</button>');
        var $b_reorder = $('<button class="b_reorder btn">Reordenar</button>');
        $botonera
            .append($b_clean)
            .append($b_load)
            .append($b_filter)
            .append($b_reorder);

        $container.append($botonera);

        clean = _clean.bind(this);
        render = _render.bind(this);
       // reorder = _reorder.bind(this);

        $b_clean.on('click', clean);
        $b_load.on('click', render);
        $b_filter.on('click', filter.bind(this));
        $b_reorder.on('click', reorder.bind(this));
    }

    function _render(){
        clean();
        collection.map(function(item){
            var $img = $('<img>')
                .attr('src', item.bird_image);
            var $text = $('<h3></h3>')
                .text(item.bird_name);
            var muyVisto = item.bird_sightings > 10 ? 1:0;
            var $li = $('<li></li>')
                .append($img)
                .append($text)
                .attr('data-ref', item.id)
                .attr('data-vistas', item.bird_sightings)
                .attr('data-many', muyVisto)
                .on('click', abreModal)
            $ul.append($li);
        });
    }

    function abreModal(){
        var copia = $(this).clone();
        copia.modal();
    }

    function _clean(){
        console.log(this);
        $('li', this).off('click', abreModal)
            .remove();
    }

    function filter(){
        $('li[data-many="1"]').toggleClass('hidden');
    }

    function quickSort (_lista){
        if(_lista.length < 2){
            return _lista;
        }
        var left = [];
        var right = [];
        var newArray = [];
        var pivot = _lista.pop();
        var length = _lista.length;

        for(var i=0; i<_lista.length; i++){
            if(Number(_lista[i].bird_sightings) <= Number(pivot.bird_sightings)){
                left.push(_lista[i])
            } else {
                right.push(_lista[i])
            }
        }

        return newArray.concat(quickSort(left), pivot, quickSort(right));
    }
    function reorder(){
        collection = quickSort(collection);
        render();
    }

})($)
$( document ).ready(function() {
    console.log("Página cargada");

    lista = [];
    listaDesordenada = [];
    var tope = 100000;
    for(var i = 0; i<tope; i++){
        lista.push(i);
        listaDesordenada.push(Math.round(tope * Math.random()));
    }
    var searchValue = lista[Math.floor(lista.length * Math.random())];

    var now = Date.now();
    for(var i = 0; i<tope; i++) {
        if(lista[i] == searchValue){
            console.log("Encontrado!");
            break;
        }
    }
    var time = Date.now() - now;
    console.log("Ha necesitado " + time);



    now = Date.now();
    search = function(lista, searchValue){
        var low = 0;
        var high = lista.length - 1;
        while(low <=high){
            var middle = Math.floor((low + high)/2);
            var guess = lista[middle];
            if(guess == searchValue){
                console.log("Encontrado!");
                break;
            }
            if(guess > searchValue){
                high = middle - 1;
            }else{
                low = middle + 1;
            }
        }
        return -1;
    }
    search(lista, searchValue);
    var time = Date.now() - now;
    console.log("Ha necesitado " + time);



    now = Date.now();
    bubbleSort = function(_lista){
        var temp = 0;
        for(var i=0; i<_lista.length; i++){
            for(var j=0; j<i; j++){
                if(_lista[j] > _lista[j+1]){
                    temp = _lista[j];
                    _lista[j] = _lista[j + 1];
                    _lista[j + 1] = temp;
                }
            }
        }
        return _lista;
    }
    listaOrdenada = bubbleSort(listaDesordenada.slice());
    var time = Date.now() - now;
    console.log("Ha necesitado " + time);



    now = Date.now();
    quickSort = function(_lista){
        if(_lista.length < 2){
            return _lista;
        }
        var left = [];
        var right = [];
        var newArray = [];
        var pivot = _lista.pop();
        var length = _lista.length;

        for(var i=0; i<_lista.length; i++){
            if(_lista[i] <= pivot){
                left.push(_lista[i])
            } else {
                right.push(_lista[i])
            }
        }

        return newArray.concat(quickSort(left), pivot, quickSort(right));
    }
    listaOrdenada = quickSort(listaDesordenada.slice());
    var time = Date.now() - now;
    console.log("Ha necesitado " + time);
});

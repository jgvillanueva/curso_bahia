$( document ).ready(function() {
    console.log('Página cargada');


});

/*
alcance
 */
var globalVar = 'variable global',
    miVariable = 'variable global miVariable';

function testVariables() {
    var localVar = 'variable local',
        miVariable = 'miVariable en local';

    console.log('globalVar', globalVar);
    console.log('localVar', localVar);

    console.log('miVariable', miVariable);
}
testVariables();
console.log('globalVar', globalVar);
//console.log('localVar', localVar);

console.log('miVariable', miVariable);


/*
contexto
 */
var value = 'Valor global';
var internalData = {
    value: 'Valor interno',
    getValueExterno: function(){
        return value;
    },
    getValueInterno: function(){
        return this.value;
    }
}
console.log(internalData.getValueInterno());
console.log(internalData.getValueExterno());


/*
funciones autoejecutables
 */
(function (window){
    window.utilidades = this;

    this.log = function(value){
        var now = new Date();
        var prefix = 'Logger: ' + now.getHours() + ':' + now.getMinutes()  + ':' + now.getSeconds();
        console.log(prefix, value);
    }
})(window)


window.utilidades.log('Hola mundo');
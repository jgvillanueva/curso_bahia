(function(window){
    window.logger = this;
        this.log = function(text){
            var now = new Date();
            var nowText = "Logger: " + now.getHours() + ":" + now.getMinutes() + ":" +now.getSeconds() + ": ";
            console.log(nowText + text);
        }

})(window)